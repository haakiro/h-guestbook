*************** copy file from remote **
php -r "copy('https://symfony.com/favicon.ico', 'public/favicon.ico');"

*************** symfony directory concept ***********
The bin/ directory contains the main CLI entry point: console. You will use it all the time.

The config/ directory is made of a set of default and sensible configuration files. One file per package. You will barely change them, trusting the defaults is almost always a good idea.

The public/ directory is the web root directory, and the index.php script is the main entry point for all dynamic HTTP resources.

The src/ directory hosts all the code you will write; that’s where you will spend most of your time. By default, all classes under this directory use the App PHP namespace. It is your home. Your code. Your domain logic. Symfony has very little to say there.

The var/ directory contains caches, logs, and files generated at runtime by the application. You can leave it alone. It is the only directory that needs to be writable in production.

The vendor/ directory contains all packages installed by Composer, including Symfony itself. That’s our secret weapon to be more productive. Let’s not reinvent the wheel. You will rely on existing libraries to do the hard work. The directory is managed by Composer. Never touch it.

********************** initialize symfony cloud project ********


***************** add dependecie : profiler *****
symfony composer req profiler --dev

********************
Switching from one environment to another can be done by changing the APP_ENV environment variable.

***************
symfony composer req logger
***
symfony composer req debug --dev

*******To manage annotations, we need to add another dependency: ********
symfony composer req annotations

**********Create your first Controller via the make:controller command: *********
symfony console make:controller ConferenceController

**** req orm doctrine **
symfony composer req "orm:2"

**************** Create the db specified in the.env file ********
symfony console doctrine:database:create

********* make migrations ***
synfony console make:migration

****************** updating the local database agter migration **
symfony console doctrine:migrations:migrate
